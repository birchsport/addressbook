package sample.addressbook;

import java.util.Calendar;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		
		Person jim = Utilities.createPerson("James", "Birchfield", 1973,
				Calendar.SEPTEMBER, 14, "123456789");
		Person ron = Utilities.createPerson("ron", "wallace", 1990,
				Calendar.MARCH, 15, "319000000");
		Address jimHomeAddress = Utilities.createAddress("205 N. Cedar ST", "",
				"La Grange", "KY", "40031", Address.Type.HOME);
		Address jimWorkAddress = Utilities.createAddress("205 N. Cedar ST", "",
				"La Grange", "KY", "40031", Address.Type.WORK);
		PhoneNumber jimHomeNumber = Utilities.createPhoneNumber("4434725703",
				PhoneNumber.Type.HOME);
		PhoneNumber jimCellNumber = Utilities.createPhoneNumber("4434725703",
				PhoneNumber.Type.CELL);
		PhoneNumber createPhoneNumber = Utilities.createPhoneNumber(
				"5023657909", PhoneNumber.Type.HOME);

		jim.addAddress(jimHomeAddress);
		jim.addAddress(jimWorkAddress);
		jim.addPhoneNumber(jimHomeNumber);
		jim.addPhoneNumber(jimCellNumber);

		AddressBook addressBook = AddressBook.loadAddressBook();
		addressBook.addPerson(ron);
		addressBook.addPerson(jim);
		AddressBook.save(addressBook);

		System.out.println(addressBook);

		List<Person> people = addressBook.findPeople("ame");
		System.out.println(people);

	}
}

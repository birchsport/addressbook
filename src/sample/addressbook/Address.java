package sample.addressbook;

import java.io.Serializable;

public class Address implements Serializable {

    enum Type {
        HOME, WORK
    }

    /**
     *
     */
    private static final long serialVersionUID = -6635756528450310465L;

    private String street1;
    private String street2;
    private String city;
    private String state;
    private String zipCode;

    private Type type = Type.HOME;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Address other = (Address) obj;
        if (city == null) {
            if (other.city != null) {
                return false;
            }
        } else if (!city.equals(other.city)) {
            return false;
        }
        if (state == null) {
            if (other.state != null) {
                return false;
            }
        } else if (!state.equals(other.state)) {
            return false;
        }
        if (street1 == null) {
            if (other.street1 != null) {
                return false;
            }
        } else if (!street1.equals(other.street1)) {
            return false;
        }
        if (street2 == null) {
            if (other.street2 != null) {
                return false;
            }
        } else if (!street2.equals(other.street2)) {
            return false;
        }
        if (type != other.type) {
            return false;
        }
        if (zipCode == null) {
            if (other.zipCode != null) {
                return false;
            }
        } else if (!zipCode.equals(other.zipCode)) {
            return false;
        }
        return true;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getStreet1() {
        return street1;
    }

    public String getStreet2() {
        return street2;
    }

    public Type getType() {
        return type;
    }

    public String getZipCode() {
        return zipCode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((city == null) ? 0 : city.hashCode());
        result = (prime * result) + ((state == null) ? 0 : state.hashCode());
        result = (prime * result)
                + ((street1 == null) ? 0 : street1.hashCode());
        result = (prime * result)
                + ((street2 == null) ? 0 : street2.hashCode());
        result = (prime * result) + ((type == null) ? 0 : type.hashCode());
        result = (prime * result)
                + ((zipCode == null) ? 0 : zipCode.hashCode());
        return result;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return "Address [street1=" + street1 + ", street2=" + street2
                + ", city=" + city + ", state=" + state + ", zipCode="
                + zipCode + ", type=" + type + "]";
    }

}

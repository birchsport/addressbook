package sample.addressbook;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.management.RuntimeErrorException;

import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;
import org.joda.time.Years;

public class Person implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5422357277869906016L;
    private String firstName = "";
    private String lastName = "";
    private int age = 0;
    private Date birthDate;
    private Set<Address> addresses = new HashSet<Address>();
    private Set<PhoneNumber> phoneNumbers = new HashSet<PhoneNumber>();
    private String socialSecurityNumber = "";

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        int length = socialSecurityNumber.length();
        if (length != 9) {
            throw new RuntimeException(
                    "Social Security Number must be 9 digits in length!");

        }
        char[] charArray = socialSecurityNumber.toCharArray();
        for (char c : charArray) {
            boolean digit = Character.isDigit(c);
            if (!digit) {
                throw new RuntimeException(
                        "socialSecurityNumber must be all digits");
            }
        }
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public void addPhoneNumber(PhoneNumber phoneNumber) {
        phoneNumbers.add(phoneNumber);
    }

    public Set<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(Set<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void addAddress(Address address) {
        this.addresses.add(address);
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;

        DateTime now = new DateTime();
        DateTime then = new DateTime(birthDate.getTime());
        Years yearsBetween = Years.yearsBetween(then, now);
        age = yearsBetween.get(DurationFieldType.years());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((addresses == null) ? 0 : addresses.hashCode());
        result = prime * result + age;
        result = prime * result
                + ((birthDate == null) ? 0 : birthDate.hashCode());
        result = prime * result
                + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result
                + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result
                + ((phoneNumbers == null) ? 0 : phoneNumbers.hashCode());
        result = prime
                * result
                + ((socialSecurityNumber == null) ? 0 : socialSecurityNumber
                        .hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Person other = (Person) obj;
        if (addresses == null) {
            if (other.addresses != null)
                return false;
        } else if (!addresses.equals(other.addresses))
            return false;
        if (age != other.age)
            return false;
        if (birthDate == null) {
            if (other.birthDate != null)
                return false;
        } else if (!birthDate.equals(other.birthDate))
            return false;
        if (firstName == null) {
            if (other.firstName != null)
                return false;
        } else if (!firstName.equals(other.firstName))
            return false;
        if (lastName == null) {
            if (other.lastName != null)
                return false;
        } else if (!lastName.equals(other.lastName))
            return false;
        if (phoneNumbers == null) {
            if (other.phoneNumbers != null)
                return false;
        } else if (!phoneNumbers.equals(other.phoneNumbers))
            return false;
        if (socialSecurityNumber == null) {
            if (other.socialSecurityNumber != null)
                return false;
        } else if (!socialSecurityNumber.equals(other.socialSecurityNumber))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Person [firstName=" + firstName + ", lastName=" + lastName
                + ", age=" + age + ", birthDate=" + birthDate + ", addresses="
                + addresses + ", phoneNumbers=" + phoneNumbers
                + ", socialSecurityNumber=" + socialSecurityNumber + "]";
    }

}

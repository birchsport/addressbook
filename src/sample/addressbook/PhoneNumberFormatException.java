package sample.addressbook;

public class PhoneNumberFormatException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 6804616706274871343L;

    public PhoneNumberFormatException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public PhoneNumberFormatException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public PhoneNumberFormatException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public PhoneNumberFormatException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

    public PhoneNumberFormatException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}

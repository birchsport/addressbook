package sample.addressbook;

import java.io.Serializable;

public class PhoneNumber implements Serializable {

    enum Type {
        HOME, WORK, CELL
    }

    /**
     *
     */
    private static final long serialVersionUID = -3061929571434915568L;

    private String phoneNumber = "";

    private Type type = Type.HOME;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PhoneNumber other = (PhoneNumber) obj;
        if (phoneNumber == null) {
            if (other.phoneNumber != null) {
                return false;
            }
        } else if (!phoneNumber.equals(other.phoneNumber)) {
            return false;
        }
        if (type != other.type) {
            return false;
        }
        return true;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Type getType() {
        return type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result)
                + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        result = (prime * result) + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    public void setPhoneNumber(String phoneNumber) {
        if ((phoneNumber == null) || (phoneNumber.length() != 10)) {
            throw new PhoneNumberFormatException(
                    "Phone number must be 10 digits");
        }
        String areaCode = phoneNumber.substring(0, 3);
        String prefix = phoneNumber.substring(3, 6);
        String last = phoneNumber.substring(6);
        this.phoneNumber = "(" + areaCode + ") " + prefix + "-" + last;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "PhoneNumber [phoneNumber=" + phoneNumber + ", type=" + type
                + "]";
    }
}

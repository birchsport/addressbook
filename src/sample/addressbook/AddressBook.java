package sample.addressbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AddressBook implements Serializable {

    public static final String DEFAULT_LOCATION = System
            .getProperty("user.home") + "/addressbook.ser";

    /**
     *
     */
    private static final long serialVersionUID = -1344455858770563831L;

    public static AddressBook loadAddressBook() {
        return AddressBook.loadAddressBook(DEFAULT_LOCATION);
    }

    public static AddressBook loadAddressBook(String path) {
        AddressBook addressBook = null;
        File file = new File(path);
        if (file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(fis);
                addressBook = (AddressBook) ois.readObject();
                System.out.println("Loaded address book from " + path);
                return addressBook;
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            addressBook = new AddressBook();
            System.out.println("Created a new address book.");
        }
        return addressBook;
    }

    public static void save(AddressBook addressBook) {
        File file = new File(DEFAULT_LOCATION);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(addressBook);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("Saved address book to " + DEFAULT_LOCATION);
    }

    public static void save(AddressBook addressBook, String path) {
        AddressBook.save(addressBook, DEFAULT_LOCATION);
    }

    private List<Person> people = new ArrayList<Person>();

    public void addPerson(Person person) {
        people.add(person);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AddressBook other = (AddressBook) obj;
        if (people == null) {
            if (other.people != null) {
                return false;
            }
        } else if (!people.equals(other.people)) {
            return false;
        }
        return true;
    }

    public List<Person> findPeople(String regex) {
        List<Person> found = new ArrayList<Person>();
        for (Person person : this.people) {
            if (person.getFirstName().contains(regex)
                    || person.getLastName().contains(regex)) {
                found.add(person);
            }
        }
        return found;
    }

    public List<Person> getPeople() {
        return people;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((people == null) ? 0 : people.hashCode());
        return result;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

    @Override
    public String toString() {
        return "AddressBook [people=" + people + "]";
    }
}

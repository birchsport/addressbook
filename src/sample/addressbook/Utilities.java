package sample.addressbook;

import java.util.Calendar;
import java.util.Date;

public class Utilities {

    public static Address createAddress(String street1, String street2,
            String city, String state, String zipCode, Address.Type type) {
        Address address = new Address();
        address.setStreet1(street1);
        address.setStreet2(street2);
        address.setCity(city);
        address.setState(state);
        address.setZipCode(zipCode);
        address.setType(type);
        return address;
    }

    public static Person createPerson(String firstName, String lastName,
            int year, int month, int day, String socialSecurityNumber) {
        Person person = new Person();
        person.setFirstName(firstName);
        person.setLastName(lastName);
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        Date birthDate = calendar.getTime();
        person.setBirthDate(birthDate);
        person.setSocialSecurityNumber(socialSecurityNumber); 
        return person;
    }

    public static PhoneNumber createPhoneNumber(String number,
            PhoneNumber.Type type) {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setPhoneNumber(number);
        phoneNumber.setType(type);
        return phoneNumber;
    }

}

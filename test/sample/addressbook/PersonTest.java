package sample.addressbook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class PersonTest {

    @Test
    public void testAddAddress() {
        Person jim = Utilities.createPerson("James", "Birchfield", 1973,
                Calendar.SEPTEMBER, 14, "123456789");
        Address jimHomeAddress = Utilities.createAddress("205 N. Cedar ST", "",
                "La Grange", "KY", "40031", Address.Type.HOME);
        Address jimWorkAddress = Utilities.createAddress("205 N. Cedar ST", "",
                "La Grange", "KY", "40031", Address.Type.WORK);
        jim.addAddress(jimHomeAddress);
        jim.addAddress(jimWorkAddress);

        Set<Address> addresses = jim.getAddresses();
        assertEquals(2, addresses.size());
        assertTrue(addresses.contains(jimHomeAddress));
        assertTrue(addresses.contains(jimWorkAddress));
    }

    @Test
    public void testAddPhoneNumber() {
        Person jim = Utilities.createPerson("James", "Birchfield", 1973,
                Calendar.SEPTEMBER, 14, "123456789");
        PhoneNumber jimHomeNumber = Utilities.createPhoneNumber("4434725703",
                PhoneNumber.Type.HOME);
        PhoneNumber jimCellNumber = Utilities.createPhoneNumber("4434725703",
                PhoneNumber.Type.CELL);
        jim.addPhoneNumber(jimHomeNumber);
        jim.addPhoneNumber(jimCellNumber);

        Set<PhoneNumber> phoneNumbers = jim.getPhoneNumbers();
        assertEquals(2, phoneNumbers.size());
        assertTrue(phoneNumbers.contains(jimHomeNumber));
        assertTrue(phoneNumbers.contains(jimCellNumber));
    }

    @Test
    public void testGetAge() {
        Person jim = Utilities.createPerson("James", "Birchfield", 1973,
                Calendar.SEPTEMBER, 14, "123456789");

        assertEquals(40, jim.getAge());
    }

}
